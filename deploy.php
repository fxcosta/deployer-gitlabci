<?php

namespace Deployer;

require 'recipe/symfony.php';

// Project name
set('application', 'DeployerGitlabCI');

// Project repository
set('repository', 'git@gitlab.com:joubertredrat/deployer-gitlabci.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('bin_dir', 'bin');
set('var_dir', 'var');
set('writable_use_sudo', false);
set('git_tty', false);

// Shared files/dirs between deploys
add(
    'shared_files',
    [
        'app/config/parameters.yml',
    ]
);
add(
    'shared_dirs',
    [
        'var/logs',
        'var/sessions',
    ]
);

// Writable dirs by web server
add('writable_dirs', []);

// Hosts
host('production')
    ->hostname('app.example.redrat.com.br')
    ->user('app')
    ->port(22)
    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->stage('production')
    ->set('deploy_path', '/home/app/public_html/app.example.redrat.com.br')
    ->set('branch', 'master')
;

// Tasks
task('build', function () {
    run('cd {{release_path}} && build');
});

task('deploy:writable', function () {

});

task('deploy:vendors:yarn', function () {
    run('cd {{release_path}} && yarn install');
});

task('deploy:assets:yarn', function () {
    run('bash {{release_path}}/scripts/publish_assets_web.sh');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

after('deploy:symlink', 'deploy:vendors:yarn');

after('deploy:vendors:yarn', 'deploy:assets:yarn');
